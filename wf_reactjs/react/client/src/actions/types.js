export const FETCH_WF_LATEST = 'fetch_wf_latest';
export const FETCH_SURVEYS = 'fetch_surveys';
export const FETCH_WEATHER = 'fetch_weather';
export const FETCH_INITIAL = 'fetch_initial';
