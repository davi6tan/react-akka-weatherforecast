import {FETCH_WF_LATEST, FETCH_SURVEYS} from './types';
import * as api from "./api";

//1
export const fetchWfLatest = () => async dispatch => {
    const res = await api.fetchWithRoute(`api/wflatest`);
    dispatch({type: FETCH_WF_LATEST, payload: res.data});
};

//3
export const submitSurvey = (values, history) => async dispatch => {
    const {city, temperature} = values;
    try {
        await api.postWeather(`${city}/${temperature}`, values);
        history.push('/');
        // fetchWfLatest();

    } catch (err) {
        console.error(err)
    }
};
// next fetch latest

// get one
export const fetchSurveys = () => async dispatch => {
    const res = await api.fetchWithRoute(`api/wf`);
    dispatch({type: FETCH_SURVEYS, payload: res.data});
};
