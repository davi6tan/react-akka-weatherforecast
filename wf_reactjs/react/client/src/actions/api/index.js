import axios from 'axios';
import {API_URL, API_URL11000} from './constants';
import fetch from "isomorphic-fetch";

const client9000 = axios.create({
    baseURL: API_URL,
    headers: {
        'Content-Type': 'application/json',
    },
});

function postByFetch(url, values) {

    return fetch(url,
        {
            method: 'POST',
            headers: {
                Accept: 'application/json',
            },
            body: JSON.stringify(values)
        }
    ).then(res => res);
}

export function fetchWithRoute(route) {
    return client9000.get(`/${route}`);
}

export function postWeather(route, values) {
    return postByFetch(`${API_URL11000}/wf/${route}`, values);

}


