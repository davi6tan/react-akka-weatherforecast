import { FETCH_WF_LATEST } from '../actions/types';

const INIT = {
   name:'David',
   others:[],
   city: 'Chicago',
    temperature: 45
}

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_WF_LATEST:
        const {city,temperature} = action.payload;
        return {...state, INIT, city, temperature};
    default:
      return state;
  }
}
/*
export const fetchWfLatest = () => async dispatch => {
  const res = await axios.get('https://ih3hqd3lg6.execute-api.us-west-2.amazonaws.com/dev/api/users');
  dispatch({ type: FETCH_WF_LATEST, payload: res.data });
};
 */