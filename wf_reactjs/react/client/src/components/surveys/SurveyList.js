import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchSurveys } from '../../actions';

class SurveyList extends Component {
  componentDidMount() {
    this.props.fetchSurveys();
  }

  renderSurveys() {
    return this.props.surveys.reverse().map((survey,index) => {
      return (
        <div className="card darken-1" key={index}>
          <div className="card-content">
            <span className="card-title">{survey.city}</span>
            <p>
              {survey.body}
            </p>
            <p className="right">
              Temperature: {survey.temperature}
            </p>
          </div>
          <div className="card-action">
            <a>Yes: {index}</a>
            <a>No: {survey.no}</a>
          </div>
        </div>
      );
    });
  }

  render() {
    return (
      <div>
        {this.renderSurveys()}
      </div>
    );
  }
}

function mapStateToProps({ surveys }) {
  return { surveys };
}

export default connect(mapStateToProps, { fetchSurveys })(SurveyList);
