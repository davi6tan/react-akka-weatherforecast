import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

class Header extends Component {
    renderContent() {
        const {auth} = this.props;
        const {city,temperature} = auth;

        switch (this.props.auth) {
            default:
                return [
                    <li key="1">{city}
                        <i className="material-icons right">location_city</i>
                    </li>,
                    <li key="2">{temperature}
                        <i className="material-icons right">ac_unit</i>
                    </li>,
                    <li key="3" style={{margin: '0 10px'}}>
                        Latest
                        {/*put here the values*/}
                    </li>
                ];
        }
    }

    render() {
        return (
            <nav className="blue darken-4">
                <div className="nav-wrapper">
                    <Link
                        to={this.props.auth ? '/surveys' : '/'}
                        className="brand-logo left"
                    >
                        Weather
                    </Link>
                    <ul className="right">
                        {this.renderContent()}
                    </ul>
                </div>
            </nav>
        );
    }
}

//cloud_queue

function mapStateToProps({auth}) {
    return {auth};
}

export default connect(mapStateToProps)(Header);
